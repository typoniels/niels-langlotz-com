// Gatsby supports TypeScript natively!
import React from "react"
import { PageProps, Link, graphql } from "gatsby"
import Layout from "../components/layout"
import SEO from "../components/seo"
import { rhythm } from "../utils/typography"

type Data = {
  site: {
    siteMetadata: {
      title: string
    }
  }
}

const Impressum = ({ data, location }: PageProps<Data>) => {
  const siteTitle = data.site.siteMetadata.title

  return (
    <Layout location={location} title={siteTitle}>
      <SEO title="Impressum" />
        <div class="col-md-8">
            <div id="c170" class="frame frame-default frame-type-textmedia frame-layout-0">
                <div class="ce-textpic ce-center ce-above">
                    <div class="ce-gallery" data-ce-columns="1" data-ce-images="1">
                        <div class="ce-outer">
                            <div class="ce-inner">
                                <div class="ce-row">
                                    <div class="ce-column">
                                        <figure class="image">
                                            <img class="image-embed-item img-fluid" src="/rechtliches_links.jpg" width="730" height="207" alt=""/>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <header><h1 class=""> Impressum </h1></header>
                    <div class="ce-bodytext"><p>Inhaltlich und technisch wird diese Webseite betrieben und bereut
                        von:</p>
                        <p><strong>typoNiels&nbsp;&nbsp;- Webentwickler Halle (Saale)</strong><br/> Professionelle
                            Internetlösungen aus Halle an der Saale</p>
                        <p>V.i.S.d.P.</p>
                        <p>Niels Langlotz<br/> Mediengestalter Digital und Print, Frontend-Design, TYPO3-Integrator
                        </p>
                        <p>Merseburger Straße 323<br/> 06132 Halle (Saale)</p>
                        <p>E-Mail: <a href="javascript:linkTo_UnCryptMailto('ocknvq,kphqBvarqpkgnu0fg');">info(at)typoniels.de</a><br/>
                            Web: <a href="https://www.typoniels.de/" target="_blank">https://www.typoniels.de</a>
                        </p>
                        <p>Als Kleinunternehmer im Sinne von § 19 Abs. 1 UStG wird Umsatzsteuer nicht
                            ausgewiesen.</p>
                        <hr/>
                        <p><strong><del>Redaktionssystem TYPO3</del> ReactJS + Gatsby</strong></p>
                        <p>Diese Webseite <del>wird</del> wurde mit der aktuellen LTS-Version des Content Management System TYPO3 betrieben. Das System wurde durch eigens entwickelte Erweiterungen individualisiert.</p><p>Nach einem Softrelaunch basiert sie jetzt auf einem React-Projekt mit dem Gatsby Generator und einem markdownbasierten Blog.</p></div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div id="contact">
                <div class="frame frame-default frame-type-textmedia frame-layout-0">
                    <div class="ce-textpic ce-center ce-above">
                        <div class="ce-gallery" data-ce-columns="1" data-ce-images="1">
                            <div class="ce-outer">
                                <div class="ce-inner">
                                    <div class="ce-row">
                                        <div class="ce-column">
                                            <figure class="image"><img src="/rechtliches_sidebar.jpg"
                                                width="480" height="252" class="image-embed-item img-fluid"
                                                alt="" /></figure>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <header><h2>Rechtliche Informationen</h2>
                            <p style={{paddingTop:'10px'}}>Hier finden Sie alle Informationen zum Betreiber und zum
                                Datenschutz.</p></header>
                        <ul class="list-group">
                            <li class="list-group-item " style={{display: 'inline-block'}}>
                                <Link
                                    class="nav-link text-secondary"
                                    activeClassName="nav-link active"
                                    to={`/impressum`}>
                                    Impressum
                                </Link>
                            </li>
                            <li class="list-group-item " style={{display: 'inline-block'}}>
                                <Link
                                    class="nav-link text-secondary"
                                    activeClassName="nav-link active"
                                    to={`/datenschutz`}>
                                    Datenschutz
                                </Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </Layout>
  )
}

export default Impressum

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
  }
`
