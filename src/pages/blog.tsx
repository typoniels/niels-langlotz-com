// Gatsby supports TypeScript natively!
import React from "react"
import {PageProps, Link, graphql} from "gatsby"

import Bio from "../components/bio"
import Layout from "../components/layout"
import SEO from "../components/seo"
import {rhythm} from "../utils/typography"

type Data = {
    site: {
        siteMetadata: {
            title: string
        }
    }
    allMarkdownRemark: {
        edges: {
            node: {
                excerpt: string
                image: string
                frontmatter: {
                    title: string
                    date: {
                        formatString: "D MMMM, YYYY"
                    }
                    description: string
                }
                fields: {
                    slug: string
                }
            }
        }[]
    }
}

const BlogIndex = ({data, location}: PageProps<Data>) => {
    const siteTitle = data.site.siteMetadata.title
    const posts = data.allMarkdownRemark.edges
    return (
        <Layout location={location} title={siteTitle}>
            <SEO title="Alle Beiträge"/>
            {/* <Bio /> */}
            <div class="col-md-8">
                <div id="c170" class="frame frame-default frame-type-textmedia frame-layout-0">
                    <div class="ce-textpic ce-center ce-above">
                        <div class="ce-gallery" data-ce-columns="1" data-ce-images="1">
                            <div class="ce-outer">
                                <div class="ce-inner">
                                    <div class="ce-row">
                                        <div class="ce-column">
                                            <figure class="image">
                                                <img class="image-embed-item img-fluid" src="/Blog.jpg" width="730"
                                                     height="207" alt=""/>
                                            </figure>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <header>
                            <h1>
                                Willkommen in meinem Blog
                            </h1>
                        </header>
                        <div class="ce-bodytext"><p><strong>Schön, dass Du auf meiner Blogseite gelandet bist.</strong>
                        </p>
                            <p>Mein Name ist Niels Langlotz, ich bin Webentwickler und befasse mich leidenschaftlich mit
                                Trends rund um die Themen Webentwicklung, Fotografie, IT-Management und UX-Design.</p>
                        </div>
                        <div className="bloglist pt-3 bg-dark">
                            {posts.map(({node}) => {
                                const title = node.frontmatter.title || node.fields.slug
                                return (
                                    <article key={node.fields.slug}>
                                        <header>
                                            <h3 class="h4 text-info text-white" style={{marginBottom: rhythm(1 / 4),}}>
                                                <Link class="text-white" style={{boxShadow: `none`}}
                                                      to={node.fields.slug}>
                                                    {title}
                                                </Link>
                                            </h3>

                                            <p class="text-white mb-0 pb-1">
                                                <span class="text-white-50">{node.frontmatter.date} | </span>
                                                <span class="mb-0 text-white-50" dangerouslySetInnerHTML={{
                                                    __html: node.frontmatter.description || node.excerpt,

                                                }}
                                                />
                                            </p>
                                        </header>
                                        <hr class="d-block mt-3 mb-0"/>
                                    </article>
                                )
                            })}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div id="contact">
                    <div class="frame frame-default frame-type-textmedia frame-layout-0">
                        <div class="ce-textpic ce-center ce-above">
                            <div class="ce-gallery" data-ce-columns="1" data-ce-images="1">
                                <div class="ce-outer">
                                    <div class="ce-inner">
                                        <div class="ce-row">
                                            <div class="ce-column">
                                                <figure class="image"><img src="/rechtliches_sidebar.jpg"
                                                                           width="480" height="252"
                                                                           class="image-embed-item img-fluid"
                                                                           alt=""/></figure>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <header><h2>Mehr von TYPONiels</h2>
                                <p style={{paddingTop: '10px'}}>Weitere Inhalte finden Sie hier...</p></header>
                            <ul class="list-group">
                                <li class="list-group-item " style={{display: 'inline-block'}}>
                                    <a class="nav-link text-secondary" href="https://thinkdigital.typoniels.de" target="_blank">
                                        ThinkDigital
                                    </a>
                                </li>
                                <li class="list-group-item " style={{display: 'inline-block'}}>
                                    <a class="nav-link text-secondary" href="https://radar.typoniels.de" target="_blank">
                                        TechRadar
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    )
}

export default BlogIndex

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    allMarkdownRemark(sort: { fields: [frontmatter___date], order: DESC }) {
      edges {
        node {
          excerpt
          fields {
            slug
          }
          frontmatter {
            date(formatString: "DD MMMM YYYY" locale: "de-DE")
            title
            description
          }
        }
      }
    }
  }
`
