// Gatsby supports TypeScript natively!
import React from "react"
import {PageProps, Link, graphql} from "gatsby"

import Bio from "../components/bio"
import Layout from "../components/layout"
import SEO from "../components/seo"
import {rhythm} from "../utils/typography"

type Data = {
    site: {
        siteMetadata: {
            title: string
        }
    }
    allMarkdownRemark: {
        edges: {
            node: {
                excerpt: string
                image: string
                frontmatter: {
                    title: string
                    date: {
                        formatString: "D MMMM, YYYY"
                    }
                    description: string
                }
                fields: {
                    slug: string
                }
            }
        }[]
    }
}

const Index = ({data, location}: PageProps<Data>) => {
    const siteTitle = data.site.siteMetadata.title
    const posts = data.allMarkdownRemark.edges

    return (
        <Layout location={location} title={siteTitle}>
            <SEO title="Herzlich Willkommen auf meiner Website" description="Mein Name ist Niels Langlotz, ich bin passionierter Webentwickler und befasse mich gerne mit allen Trends rund um die Themen Webentwicklung, Fotografie, IT-Management und UX-Design." />
            <div className="col-md-8">
                <div id="c161" className="frame frame-default frame-type-textmedia frame-layout-0">
                    <div className="ce-textpic ce-center ce-above">
                        <div className="ce-gallery" data-ce-columns="1" data-ce-images="1">
                            <div className="ce-outer">
                                <div className="ce-inner">
                                    <div className="ce-row">
                                        <div className="ce-column">
                                            <figure className="image"><img className="image-embed-item img-fluid"
                                                                       src="/Landingpage.jpg"
                                                                       width="730" height="208"/></figure>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <header><h1><small className={'text-dark'}>Code. Commit. Test. Deploy.</small><br/>Niels Langlotz — Webentwicklung Halle (Saale)</h1></header>
                        <div className="ce-bodytext">
                            <p>Viele Prozesse und Aufgaben im Unternehmen lassen sich durch Software digital abbilden und automatisieren. Dadurch lassen sich nicht nur viel Geld und Zeit sparen, sondern auch Fehler minimieren und schneller auf aktuelle Trends  reagieren.</p>
                            <hr/>
                            <p className={'text-info'}><strong>Als Webentwickler habe ich mich darauf spezialisiert Unternehmensprozesse mit Hilfe von
                                Weblösungen zu beschleunigen.</strong></p>
                            <ul>
                                <li><p><strong>Content Management mit TYPO3 CMS</strong><br/>Beratung,
                                    Installation, Konfiguration, Template-Integration, Anbindung an Drittsysteme, Extensionentwicklung mit Extbase &amp; Fluid, Schulungen</p>
                                </li>
                                <li><p><strong>Frontend-Entwicklung / Backend-Entwicklung</strong><br/>HTML, CSS,
                                    Javascript, Sass/Less, Bootstrap, Foundation, jQuery, NodeJS, Angular,
                                    React, PHP &amp; MySQL, Composer, Express.js, Neo4J</p>
                                </li>
                                <li><p><strong>Architektur: Alles was im Hintergrund
                                    passiert</strong><br/>Apache &amp; Nginx, Docker, Solr, Hotjar, Matomo, Sentry
                                </p></li>
                                <li><p><strong>Webdesign: Markenidentität online transportieren</strong><br/>Screendesign,
                                    Sketch/Photoshop, Prototypentwicklung, Website-Optimierungen</p></li>
                                <li><p><strong>Continuous Integration: Von der Idee bis zum Deployment</strong><br/>Version
                                    Control mit Gitlab, automatisierte Tests und Deployment, Static Pages</p></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div className="col-md-4">
                <div id="contact">
                    <div id="c162" className="frame frame-default frame-type-textmedia frame-layout-0">
                        <div className="ce-textpic ce-center ce-above">
                            <div className="ce-gallery" data-ce-columns="1" data-ce-images="1">
                                <div className="ce-outer">
                                    <div className="ce-inner">
                                        <div className="ce-row">
                                            <div className="ce-column">
                                                <figure className="image"><img className="image-embed-item img-fluid"
                                                                           src="/niels_langlotz.jpg"
                                                                           width="730" height="382"/></figure>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <header><h4>Ihr professioneller TYPO3-Webentwickler aus Halle</h4></header>
                            <div className="ce-bodytext"><p>Mein Name ist Niels Langlotz, ich bin passionierter
                                Webentwickler und befasse mich gerne mit allen Trends rund um die
                                Themen&nbsp;Webentwicklung,
                                Fotografie, IT-Management und UX-Design.</p></div>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    )
}

export default Index

export const pageQuery = graphql`
query {
    site {
    siteMetadata {
    title
}
}
    allMarkdownRemark(sort: {fields: [frontmatter___date], order: DESC}) {
    edges {
    node {
    excerpt
    fields {
    slug
}
    frontmatter {
    date(formatString: "DD MMMM YYYY" locale: "de-DE")
    title
    description
}
}
}
}
}
`