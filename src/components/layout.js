import React from "react"
import {Link} from "gatsby"

import {rhythm, scale} from "../utils/typography"
import "./layout.css"

const Layout = ({location, title, children}) => {
    const rootPath = `${__PATH_PREFIX__}/`
    let header

    if (location.pathname === rootPath) {
        header = (
            <h1
                style={{
                    ...scale(1.5),
                    marginBottom: 0,
                    marginTop: 0,
                }}
            >
                <Link
                    style={{
                        boxShadow: `none`,
                        color: `inherit`,
                    }}
                    to={`/`}
                >
                    {title}
                </Link>
            </h1>
        )
    } else {
        header = (
            <h3
                style={{
                    fontFamily: `Montserrat, sans-serif`,
                    marginTop: 0,
                    marginBottom: 0
                }}
            >
                <Link
                    style={{
                        boxShadow: `none`,
                        color: `inherit`,
                    }}
                    to={`/`}
                >
                    {title}
                </Link>
            </h3>
        )
    }
    return (
        <div>
            <section style={{background: 'white'}}>
                <div class="container-fluid" style={{background: 'white', paddingTop: '15px'}}>
                    <div class="row">
                        <div class="col-md-12">
                            <nav class="navbar navbar-expand-lg navbar-light bg-white-del"
                                 style={{marginBottom: '15px'}}>
                                <Link class="navbar-brand" activeClassName="navbar-brand" a to={'/'}>
                                    <span style={{background: '#03A9F4', color: 'white', padding: '10px'}}>Niels Langlotz</span>
                                    <span style={{
                                        background: 'transparent',
                                        padding: '10px',
                                        color: 'black'
                                    }}>Digital.</span>
                                </Link>
                                <button class="navbar-toggler" type="button" data-toggle="collapse"
                                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                        aria-expanded="false" aria-label="Toggle navigation"><span
                                    class="navbar-toggler-icon"></span></button>
                                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                    <ul class="navbar-nav mr-auto">
                                        <li class="nav-item float-right" style={{display: 'inline-block'}}>
                                            <Link
                                                class="nav-link text-secondary"
                                                activeClassName="nav-link text-dark"
                                                to={'/blog'}
                                                title="Test">
                                                Blog
                                            </Link>
                                        </li>
                                        <li class="nav-item float-right" style={{display: 'inline-block'}}>
                                            <a class="nav-link" href='https://www.typoniels.de/' target="_blank" title="Test">
                                                Corporate Website
                                            </a>
                                        </li>
                                    </ul>
                                    <ul class="navbar-nav ml-auto">
                                        <li class="nav-item " style={{display: 'inline-block'}}>
                                            <Link
                                                class="nav-link text-secondary"
                                                activeClassName="nav-link text-dark"
                                                to={`/impressum`}>
                                                Impressum
                                            </Link>
                                        </li>
                                        <li class="nav-item " style={{display: 'inline-block'}}>
                                            <Link
                                                class="nav-link text-secondary"
                                                activeClassName="nav-link text-dark"
                                                to={`/datenschutz`}>
                                                Datenschutz
                                            </Link>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </section>


            {/* <header style={{background: '#eee'}}>
                <div
                    style={{
                        marginLeft: `auto`,
                        marginRight: `auto`,
                        maxWidth: rhythm(24),
                        padding: `${rhythm(0.5)} ${rhythm(3 / 4)} 10px`,
                    }}
                >
                    {header}
                </div>
            </header>
            */}
            <main>
                <div>
                    <section class="read-more" style={{padding: '20px 0'}}>
                        <div class="container-fluid">
                            <div class="row no-gutters news-row">
                                {children}
                            </div>
                        </div>
                    </section>
                </div>
            </main>
            <footer style={{background: 'grey', color: 'white'}}>
                <div style={{
                    display: 'none'
                }}>
                    © {new Date().getFullYear()} Niels Langlotz
                </div>
            </footer>
        </div>
    )
}

export default Layout
