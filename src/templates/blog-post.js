import React from "react"
import {Link, graphql} from "gatsby"

// import Bio from "../components/bio"
import Layout from "../components/layout"
import SEO from "../components/seo"
import {rhythm, scale} from "../utils/typography"

const BlogPostTemplate = ({data, pageContext, location}) => {
    const post = data.markdownRemark
    const siteTitle = data.site.siteMetadata.title
    const {previous, next} = pageContext

    return (
        <Layout location={location} title={siteTitle}>
            <SEO
                title={post.frontmatter.title}
                description={post.frontmatter.description || post.excerpt}
            />

            <div class="col-md-8">
                <div id="c170" class="frame frame-default frame-type-textmedia frame-layout-0">
                    <article class="">
                        <header>
                            <h1 class="h3"
                                style={{
                                    marginTop: rhythm(1),
                                    marginBottom: 0,
                                }}
                            >
                                {post.frontmatter.title}
                            </h1>
                            <p
                                style={{
                                    ...scale(-1 / 5),
                                    display: `block`,
                                    marginBottom: rhythm(.6),
                                }}
                            >
                                {post.frontmatter.date}
                            </p>
                        </header>

                        <section class="px-4 pt-0" dangerouslySetInnerHTML={{__html: post.html}}/>
                        <hr
                            style={{
                                marginBottom: rhythm(1),
                            }}
                        />

                        {/*
                        <footer>
                        <Bio/>
                        </footer>
                        */}
                    </article>
                </div>
            </div>
            <div class="col-md-4">
                <div id="contact">
                    <div class="frame frame-default frame-type-textmedia frame-layout-0">
                        <div class="ce-textpic ce-center ce-above">
                            <div class="ce-gallery" data-ce-columns="1" data-ce-images="1">
                                <div class="ce-outer">
                                    <div class="ce-inner">
                                        <div class="ce-row">
                                            <div class="ce-column">
                                                <figure class="image"><img src="/rechtliches_sidebar.jpg"
                                                                           width="480" height="252"
                                                                           class="image-embed-item img-fluid"
                                                                           alt=""/></figure>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <header><h2>Dieser Blog ist noch in der Entwicklung</h2>
                                <p style={{paddingTop: '10px'}}>Bei Fehlern freue ich mich über eine kurze E-Mail an me@typoniels.de</p></header>
                            <ul class="list-group">
                                <li class="list-group-item ">
                                        <Link to={'/blog'} class="nav-link text-secondary">
                                            Zurück zur Blogliste
                                        </Link>
                                </li>
                                <li class="list-group-item ">
                                    {previous && (
                                        <Link to={previous.fields.slug} rel="prev" class="nav-link text-secondary">
                                            {previous.frontmatter.title}
                                        </Link>
                                    )}
                                </li>
                                <li class="list-group-item ">
                                    {next && (
                                        <Link to={next.fields.slug} rel="next" class="nav-link text-secondary">
                                            {next.frontmatter.title}
                                        </Link>
                                    )}
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>



        </Layout>
    )
}

export default BlogPostTemplate

export const pageQuery = graphql`
  query BlogPostBySlug($slug: String!) {
    site {
      siteMetadata {
        title
      }
    }
    markdownRemark(fields: { slug: { eq: $slug } }) {
      id
      excerpt(pruneLength: 160)
      html
      frontmatter {
        title
        date(formatString: "DD MMMM YYYY" locale: "de-DE")
        description
      }
    }
  }
`
