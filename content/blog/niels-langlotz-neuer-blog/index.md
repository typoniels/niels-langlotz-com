---
title: Mein persönlicher Blog
date: "2020-07-13"
description: ""
---

**Howdy und Willkommen im ersten Eintrag auf meinem persönlichen Blog.**

Hier soll jede Woche ein kurzer Blogbeitrag aus meinem Alltag entstehen, welcher immer die vier Themen "Wochenrückblick", "Foto der Woche", "Vorschau auf die nächste Woche" und "Lesenswertes aus den Welten des Internets" enthält.

---

#### Mein Wochenrückblick [KW29]

![GITLAB Projekt](./gitlab-repository.png)

* **Meine persönliche Website — [www.niels-langotz.com]**<br>Meine persönliche Website, welche ich 2018 als TYPO3-Präsenz gestartet habe, habe ich zur Reduzierung des Wartungs- und Ressourcenbedarfs nun in ein ReactJS Projekt (Gatsby) portiert und in meinen statischen Deploy-Workflow in Gitlab integriert. In diesem Schritt habe ich auch diesen Blog auf Markdownbasis implementiert.<br><br>
* **Meine berufliche Website — [www.typoniels.de]**<br>Die TYPONiels Präsenz habe ich auf die LTS 10.4 geupdatet und die Integration der Enterpsie Suchelösung Apache Solr insbesondere für den Blog optimiert. Der ThinkDigital Blog hat zudem eine eine neue Darstellungsform von Zitaten in der Listenansicht bekommen und die allgemeine responsive Darstellung auf kleinen Devices wurde optimiert. Jetzt funktioniert auch die Autocomplete und Ajax-Pagination :) Als nächster Schritt soll auch dieser Blog mit Solr indexiert werden. Schau dir gerne das Ergebnis unter [https://www.typoniels.de/suche](https://www.typoniels.de/suche?tx_solr%5Bq%5D=*) an.

---

#### Mein Foto der Woche "Regentag"
Bestandteil meines wöchtenlichen Blogposts soll auch immer ein Foto sein, welches in der Woche entstanden ist und für mich einen emotionalen oder symbolischen Wert hat und mir von allen entstandenen Bilder am besten gefällt.

Diese Woche ist es das Bild mit dem Titel “Regentag”, welches direkt aus meiner Wohnung entstanden ist - sehr kreativ oder?

![Regentag](./LMF_1415.jpg)

---

<!---

#### _Vorschau_: TYPO3-Distribution für Bildungseinrichtungen
Das Content Management Systen TYPO3 erfreut sich in der Industrie großer Beliebtheit.

--->

#### Lesenswertes aus den Welten des Internets

* [heise.de | Shutterstock verärgert Fotografen mit neuem Bezahlmodell](https://www.heise.de/news/Shutterstock-veraergert-Fotografen-mit-neuem-Bezahlmodell-4766659.html)