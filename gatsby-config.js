module.exports = {
  siteMetadata: {
    title: `Niels Langlotz`,
    author: {
      name: `Niels Langlotz`,
      summary: `einem TYPO3-Entwickler aus Halle (Saale).`
    },
    description: `Niels Langlotz ist TYPO3 Webentwickler aus Halle (Saale).`,
    siteUrl: `https://www.niels-langlotz.com/`,
    social: {
      website: `www.typoniels.de`
    }
  },
  plugins: [
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/content/blog`,
        name: `blog`
      }
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/content/assets`,
        name: `assets`
      }
    },
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
            {
                resolve: "gatsby-remark-external-links",
                options: {
                    target: "_blank",
                    rel: "nofollow"
                }
            },
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 700
            }
          },
          {
            resolve: `gatsby-remark-responsive-iframe`,
            options: {
              wrapperStyle: `margin-bottom: 1.0725rem`
            }
          },
          `gatsby-remark-prismjs`,
          `gatsby-remark-copy-linked-files`,
          `gatsby-remark-smartypants`
        ]
      }
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        //trackingId: `ADD YOUR TRACKING ID HERE`,
      }
    },
    `gatsby-plugin-feed`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Niels Langlotz`,
        short_name: `NL`,
        start_url: `/`,
        background_color: `#ffffff`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/favicon.png`
      }
    },
    {
          resolve: 'gatsby-plugin-load-script',
          options: {
              src: '/vendor.js',
          },
    },
    {
          resolve: 'gatsby-plugin-load-script',
          options: {
              src: '/app.js',
          },
      },
  ]
}
